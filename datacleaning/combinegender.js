// Import the package main module
const csv = require("csv-parser");
const createCsvWriter = require("csv-writer").createObjectCsvWriter;
const fs = require("fs");
const gender = require("./name_gender.json");

const csvWriter = createCsvWriter({
  path: "combinedgender.csv",
  header: [
    { id: "comments", title: "comments" },
    { id: "rating", title: "rating" },
    { id: "year", title: "year" },
    { id: "reviewer", title: "reviewer" },
    { id: "gender", title: "gender" },
    { id: "location", title: "location" },
    { id: "language", title: "language" },
  ],
});

const data = [];

fs.createReadStream("dataset.csv")
  .pipe(csv())
  .on("data", (row) => {
    // console.log(row);
    console.log(row["reviewer/first_name"]);

    let name = row["reviewer/first_name"].toLowerCase();
    let thisGender = "unknown";

    let getGender = gender.filter((x) => x.name.toLowerCase() === name);
    console.log(getGender.length);
    if (getGender.length === 1) {
      thisGender = getGender[0].gender;
    }

    console.log(thisGender);

    // let values = Object.values(row);
    let newdoc = {
      comments: row.comments,
      rating: row.rating,
      year: row.year,
      reviewer: name,
      gender: thisGender,
      location: row.city,
      language: row.language,
    };
    // let entries = Object.entries(row);
    // entries.forEach((x) => {});
    data.push(newdoc);
    // console.log(values);
    // console.log(values[3]);
  })
  .on("end", () => {
    // console.log(data);
    let finalData = data.filter(
      (x) => x.gender !== "unknown" && x.language === "en"
    );
    console.log(finalData);
    csvWriter
      .writeRecords(finalData)
      .then(() => console.log("The CSV file was written successfully"));
    console.log("CSV file successfully processed");
  });

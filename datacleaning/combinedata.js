// Import the package main module
const csv = require("csv-parser");
const createCsvWriter = require("csv-writer").createObjectCsvWriter;
const fs = require("fs");

let features = {
  location: [],
  amenity: [],
  facility: [],
  room: [],
  price: [],
  recommendation: [],
  host: [],
  security: [],
  community: [],
};

fs.createReadStream("topfeatures.csv")
  .pipe(csv())
  .on("data", (row) => {
    // console.log(row);
    if (row.theme !== "") {
      //   console.log(row.theme);
      //   console.log(features[row.theme]);
      let values = Object.values(row);
      console.log(values);
      if (features[values[2]] !== undefined) {
        features[values[2]].push(values[0]);
      }
    }
  })
  .on("end", () => {
    console.log(features);
    fs.writeFile(
      "distribution.json",
      JSON.stringify(features),
      "utf8",
      (x) => {}
    );
    console.log("CSV file successfully processed");
  });

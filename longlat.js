let axios = require("axios");
let cheerio = require("cheerio");
let fs = require("fs");
let _ = require("lodash");
let converter = require("json-2-csv");

async function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}

async function page(LONGLAT, OFFSET) {
  try {
    let SCRAPE_URL = `https://www.airbnb.ae/api/v2/explore_tabs?_format=for_explore_search_web&auto_ib=true&client_session_id=737e7786-e295-4322-a1da-07612a0011ee&currency=USD&current_tab_id=home_tab&experiences_per_grid=20&fetch_filters=true&guidebooks_per_grid=20&has_zero_guest_treatment=true&hide_dates_and_guests_filters=false&is_guided_search=true&is_new_cards_experiment=true&is_standard_search=true&items_per_grid=50&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&locale=en&metadata_only=false&ne_lat=${LONGLAT.ne_lat}&ne_lng=${LONGLAT.ne_lng}&sw_lat=${LONGLAT.sw_lat}&sw_lng=${LONGLAT.sw_lng}&query=jogja&query_understanding_enabled=true&refinement_paths%5B%5D=%2Fhomes&satori_parameters=ERIA&satori_version=1.1.0&screen_height=463&screen_size=large&screen_width=1440&search_by_map=true&search_type=unknown&selected_tab_id=home_tab&show_groupings=true&source=mc_search_bar&supports_for_you_v3=true&timezone_offset=420&version=1.6.2&zoom=16&items_offset=${OFFSET}`;

    const response = await axios.get(`${SCRAPE_URL}`);

    let data = response.data;
    // console.log(data.explore_tabs[0]);
    let explore_tabs =
      data.explore_tabs[0].sections[data.explore_tabs[0].sections.length - 1];
    if (explore_tabs === undefined) {
      return {
        data: [],
        success: true
      };
    }
    let listings = explore_tabs.listings;
    let has_next_page = data.explore_tabs[0].pagination_metadata.has_next_page;
    let items_offset = data.explore_tabs[0].pagination_metadata.items_offset;
    console.log(has_next_page, items_offset);
    if (has_next_page === true) {
      console.log("next page");
      let data = await page(LONGLAT, items_offset);
      return {
        data: listings.concat(data.data),
        success: true
      };
    } else {
      console.log(listings);
      return {
        data: [],
        success: true
      };
    }
  } catch (err) {
    console.log("RETRY");
    await wait(5000);
    return await page(LONGLAT, OFFSET);
  }
}

async function scrape() {
  let result = [];
  let INCREMENT = 1;

  //Var
  let ne_lat = -8.466169323400857;
  let ne_lng = 115.2431704798081;
  let sw_lat = -8.478245502898682;
  let sw_lng = 115.232527474437;
  let LONGLAT = {
    ne_lat,
    ne_lng,
    sw_lat,
    sw_lng
  };

  let ENDING = {
    lat: -8.539639249706507,
    lng: 115.30353527006889
  };
  let OFFSET = {
    lat: 0,
    lng: 0
  };
  let MULTIPLIER = {
    lat: Math.abs(LONGLAT.ne_lat - LONGLAT.sw_lat),
    lng: Math.abs(LONGLAT.ne_lng - LONGLAT.sw_lng)
  };
  let LIMIT = {
    lat: Math.abs(Math.floor((ENDING.lat - LONGLAT.sw_lat) / MULTIPLIER.lat)),
    lng: Math.abs(Math.floor((ENDING.lng - LONGLAT.ne_lng) / MULTIPLIER.lng))
  };
  // let LIMIT = {
  //   lat: 20,
  //   lng: 20
  // };
  console.log("Multiplier", MULTIPLIER);
  console.log("Limit", LIMIT);

  // return;

  async function cycle() {
    LONGLAT.ne_lat = ne_lat - MULTIPLIER.lat * OFFSET.lat;
    LONGLAT.ne_lng = ne_lng + MULTIPLIER.lng * OFFSET.lng;
    LONGLAT.sw_lat = sw_lat - MULTIPLIER.lat * OFFSET.lat;
    LONGLAT.sw_lng = sw_lng + MULTIPLIER.lng * OFFSET.lng;
    console.log(LONGLAT.ne_lat);
    let listings = await page(LONGLAT, 0);
    if (listings.success) {
      // result = result.concat(
      //   _.uniqBy(listings.data, x => {
      //     return x.listing.id;
      //   })
      // );
      result = result.concat(listings.data);
      result = _.uniqBy(result, x => {
        return x.listing.id;
      });
      if (OFFSET.lng < LIMIT.lng && OFFSET.lat <= LIMIT.lat) {
        OFFSET.lng = OFFSET.lng + INCREMENT;
        console.log("CONTINUE LNG", result.length, OFFSET, LIMIT, LONGLAT);
        cycle();
      } else if (OFFSET.lng >= LIMIT.lng && OFFSET.lat < LIMIT.lat) {
        OFFSET.lng = 0;
        OFFSET.lat = OFFSET.lat + INCREMENT;
        console.log("CONTINUE LAT", result.length, OFFSET, LIMIT, LONGLAT);
        cycle();
      } else if (OFFSET.lng === LIMIT.lng && OFFSET.lat === LIMIT.lat) {
        // result = result.concat(listings.data);
        // result = _.uniqBy(result, x => {
        //   return x.listing.id;
        // });
        console.log("DONE", result.length, OFFSET);
        fs.writeFile(
          "./longlat.json",
          JSON.stringify(result, null, 2),
          function(err) {}
        );
      }
    } else {
      setTimeout(x => {
        //Try Again
        cycle();
      }, 5000);
    }
  }

  cycle();
}

scrape();

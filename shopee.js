let axios = require("axios");
let cheerio = require("cheerio");
let fs = require("fs");

async function page(MATCHID, OFFSET) {
  let SCRAPE_URL = `https://shopee.co.id/api/v2/search_items/?by=pop&limit=50&match_id=${MATCHID}&newest=${OFFSET}&order=desc&page_type=search`;

  const response = await axios.get(`${SCRAPE_URL}`);

  let data = response.data;

  // console.log(data);
  return {
    count: data.total_count,
    items: data.items
  };
}

async function scrape(MATCHID) {
  let result = [];
  let OFFSET = 0;
  let INCREMENT = 50;
  let ITEMID = "1011178273";
  let SHOPID = "35026093";
  // let MATCHID = "40";
  let LIMIT = 100;

  async function cycle() {
    let listings = await page(MATCHID, OFFSET);
    console.log("ITEMS COUNT", listings.count);
    result = result.concat(listings.items);
    if (result.length < LIMIT) {
      OFFSET = OFFSET + INCREMENT;
      console.log("CONTINUE", result.length, OFFSET);
      cycle();
    } else if (result.length >= LIMIT) {
      fs.writeFile(
        "./shopee" + MATCHID + ".json",
        JSON.stringify(result, null, 2),
        function(err) {}
      );
      console.log("DONE", result.length, OFFSET);
    }
  }

  cycle();
}

scrape("40");
// scrape(41);
// scrape(42);
// scrape(43);

// Import the package main module
const csv = require("csv-parser");
const createCsvWriter = require("csv-writer").createObjectCsvWriter;
const fs = require("fs");
const features = require("./distribution.json");

let gender = "female";

const csvWriter = createCsvWriter({
  path: `topics_${gender}.csv`,
  header: [
    { id: "gender", title: "gender" },
    { id: "location", title: "location" },
    { id: "amenity", title: "amenity" },
    { id: "facility", title: "facility" },
    { id: "room", title: "room" },
    { id: "price", title: "price" },
    { id: "recommendation", title: "recommendation" },
    { id: "host", title: "host" },
    { id: "security", title: "security" },
    { id: "community", title: "community" },
  ],
});

let data = {
  gender: gender,
  location: 0,
  amenity: 0,
  facility: 0,
  room: 0,
  price: 0,
  recommendation: 0,
  host: 0,
  security: 0,
  community: 0,
};

fs.createReadStream(`${gender}_gender_topfeatures.csv`)
  .pipe(csv())
  .on("data", (row) => {
    let x = Object.values(row);
    console.log(x);
    if (features.location.includes(x[0])) {
      data.location = data.location + parseFloat(x[1]);
    }
    if (features.amenity.includes(x[0])) {
      data.amenity = data.amenity + parseFloat(x[1]);
    }
    if (features.facility.includes(x[0])) {
      data.facility = data.facility + parseFloat(x[1]);
    }
    if (features.room.includes(x[0])) {
      data.room = data.room + parseFloat(x[1]);
    }
    if (features.price.includes(x[0])) {
      data.price = data.price + parseFloat(x[1]);
    }
    if (features.recommendation.includes(x[0])) {
      data.recommendation = data.recommendation + parseFloat(x[1]);
    }
    if (features.host.includes(x[0])) {
      data.host = data.host + parseFloat(x[1]);
    }
    if (features.security.includes(x[0])) {
      data.security = data.security + parseFloat(x[1]);
    }
    if (features.community.includes(x[0])) {
      data.community = data.community + parseFloat(x[1]);
    }
  })
  .on("end", () => {
    console.log(data);
    csvWriter
      .writeRecords([data])
      .then(() => console.log("The CSV file was written successfully"));
    console.log("CSV file successfully processed");
  });

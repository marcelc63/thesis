let axios = require("axios");
let cheerio = require("cheerio");
let fs = require("fs");

let review = require("./shopeeReview.json");

async function page(SHOPID, ITEMID, OFFSET) {
  let SCRAPE_URL = `https://shopee.co.id/api/v2/item/get_ratings?filter=0&flag=1&itemid=${ITEMID}&limit=6&offset=${OFFSET}&shopid=${SHOPID}&type=0`;

  const response = await axios.get(`${SCRAPE_URL}`);

  let data = response.data;

  console.log(data);
  return {
    count: data.data.item_rating_summary.rating_total,
    items: data.data.ratings
  };
}

async function scrape() {
  let result = [];
  let OFFSET = 0;
  let INCREMENT = 50;
  let ITEMID = "2750164677";
  let SHOPID = "399334";
  // let MATCHID = "40";
  let LIMIT = 100;
  let REVIEW = review;
  // console.log(REVIEW);

  // return;
  async function cycle(SHOPID, ITEMID) {
    let listings = await page(SHOPID, ITEMID, OFFSET);
    console.log("ITEMS COUNT", listings.count);
    result = result.concat(listings.items);
    if (result.length < listings.count) {
      OFFSET = OFFSET + INCREMENT;
      console.log("CONTINUE", result.length, OFFSET);
      cycle();
    } else if (result.length >= listings.count) {
      fs.writeFile(
        "./shopeeReview" + SHOPID + ITEMID + ".json",
        JSON.stringify(result, null, 2),
        function(err) {}
      );
      console.log("DONE", result.length, OFFSET);
    }
  }

  REVIEW.forEach(x => {
    console.log(x.shopid, x.itemid);
    return;
    cycle(x.shopid, x.itemidd);
  });

  // cycle();
}

scrape();
// scrape("399334", "2750164677");
// scrape(41);
// scrape(42);
// scrape(43);

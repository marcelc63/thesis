const axios = require("axios");
const fs = require("fs");

axios({
  url:
    "https://www.airbnb.com/api/v3/ExploreSearch?locale=en&operationName=ExploreSearch&currency=USD&variables=%7B%22request%22%3A%7B%22metadataOnly%22%3Afalse%2C%22version%22%3A%221.7.6%22%2C%22itemsPerGrid%22%3A20%2C%22tabId%22%3A%22home_tab%22%2C%22checkin%22%3A%222020-07-23%22%2C%22refinementPaths%22%3A%5B%22%2Fhomes%22%5D%2C%22source%22%3A%22structured_search_input_header%22%2C%22checkout%22%3A%222020-07-31%22%2C%22searchType%22%3A%22unknown%22%2C%22neLat%22%3A%22-6.023133395497556%22%2C%22neLng%22%3A%22107.13100680175785%22%2C%22swLat%22%3A%22-6.303030653754502%22%2C%22swLng%22%3A%22106.47045382324222%22%2C%22searchByMap%22%3Atrue%2C%22query%22%3A%22Jakarta%2C%20Indonesia%22%2C%22cdnCacheSafe%22%3Afalse%2C%22simpleSearchTreatment%22%3A%22simple_search_only%22%2C%22treatmentFlags%22%3A%5B%22stays_flexible_dates%22%2C%22simple_search_1_1%22%5D%2C%22screenSize%22%3A%22large%22%7D%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%228c9b30efefd6746ad4471b4c7a38f65707d3bf6c4c0cee8cc1e45f829b734736%22%7D%7D",
  method: "get",
  headers: {
    accept: "*/*",
    "accept-encoding": "gzip, deflate, br",
    "accept-language": "en-US,en;q=0.9,id;q=0.8,ms;q=0.7,pt;q=0.6",
    "content-type": "application/json",
    origin: "https://airbnb.com",
    "sec-fetch-dest": "empty",
    "sec-fetch-mode": "cors",
    "sec-fetch-site": "same-site",
    "user-agent":
      "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36",
    "x-airbnb-api-key": "d306zoyjsyarp7ifhu67rjxn52tv0t20",
    "x-airbnb-graphql-platform": "web",
    "x-airbnb-graphql-platform-client": "apollo-niobe",
    "x-csrf-token":
      "V4$.airbnb.com$IYWsRWuBbkE$aNZNQntAV5SYbS9olXpUe6c3LL9fGB4WzWGK8XSKKSo=",
    "x-csrf-without-token": 1,
  },
})
  .then((res) => {
    console.log(res.data.data);
  })
  .catch((err) => {
    console.log(err);
  });

for (i = 1; i < 5; i++) {
  let offset = i * 20;
  axios({
    url: `https://www.airbnb.com/api/v3/ExploreSearch?locale=en&operationName=ExploreSearch&currency=USD&variables=%7B%22request%22%3A%7B%22metadataOnly%22%3Afalse%2C%22version%22%3A%221.7.6%22%2C%22itemsPerGrid%22%3A20%2C%22tabId%22%3A%22home_tab%22%2C%22checkin%22%3A%222020-07-23%22%2C%22refinementPaths%22%3A%5B%22%2Fhomes%22%5D%2C%22source%22%3A%22structured_search_input_header%22%2C%22checkout%22%3A%222020-07-31%22%2C%22searchType%22%3A%22pagination%22%2C%22neLat%22%3A%22-6.023133395497556%22%2C%22neLng%22%3A%22107.13100680175785%22%2C%22swLat%22%3A%22-6.303030653754502%22%2C%22swLng%22%3A%22106.47045382324222%22%2C%22searchByMap%22%3Atrue%2C%22federatedSearchSessionId%22%3A%2296c400d9-838a-45af-a625-426029e1fbe9%22%2C%22placeId%22%3A%22ChIJnUvjRenzaS4RILjULejFAAE%22%2C%22sectionOffset%22%3A2%2C%22itemsOffset%22%3A${offset}%2C%22query%22%3A%22Jakarta%2C%20Indonesia%22%2C%22cdnCacheSafe%22%3Afalse%2C%22simpleSearchTreatment%22%3A%22simple_search_only%22%2C%22treatmentFlags%22%3A%5B%22stays_flexible_dates%22%2C%22simple_search_1_1%22%5D%2C%22screenSize%22%3A%22large%22%7D%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%228c9b30efefd6746ad4471b4c7a38f65707d3bf6c4c0cee8cc1e45f829b734736%22%7D%7D`,
    method: "get",
    headers: {
      accept: "*/*",
      "accept-encoding": "gzip, deflate, br",
      "accept-language": "en-US,en;q=0.9,id;q=0.8,ms;q=0.7,pt;q=0.6",
      "content-type": "application/json",
      origin: "https://airbnb.com",
      "sec-fetch-dest": "empty",
      "sec-fetch-mode": "cors",
      "sec-fetch-site": "same-site",
      "user-agent":
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36",
      "x-airbnb-api-key": "d306zoyjsyarp7ifhu67rjxn52tv0t20",
      "x-airbnb-graphql-platform": "web",
      "x-airbnb-graphql-platform-client": "apollo-niobe",
      "x-csrf-token":
        "V4$.airbnb.com$IYWsRWuBbkE$aNZNQntAV5SYbS9olXpUe6c3LL9fGB4WzWGK8XSKKSo=",
      "x-csrf-without-token": 1,
    },
  })
    .then((res) => {
      fs.writeFile(
        "./AirbnbListings.json",
        JSON.stringify(res.data.data, null, 2),
        function (err) {}
      );
    })
    .catch((err) => {
      console.log(err);
    });
}

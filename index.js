let axios = require("axios");
let cheerio = require("cheerio");
let fs = require("fs");

async function page(OFFSET) {
  let SCRAPE_URL = `https://www.airbnb.com/api/v2/explore_tabs?_format=for_explore_search_web&adults=2&auto_ib=true&checkin=2019-11-01&checkout=2019-11-16&client_session_id=3fc36346-c492-4e1c-99c9-0259d38bafa5&currency=USD&current_tab_id=home_tab&experiences_per_grid=20&federated_search_session_id=d7df11c1-92ff-47dc-baa0-1f9e5eb1d85b&fetch_filters=true&guidebooks_per_grid=20&has_zero_guest_treatment=true&hide_dates_and_guests_filters=false&is_guided_search=true&is_new_cards_experiment=true&is_standard_search=true&items_offset=${OFFSET}&items_per_grid=50&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&last_search_session_id=b6a67124-9554-46f9-be53-67ccead875bc&locale=en&metadata_only=false&place_id=ChIJxWtbvYdXei4RcU9o09Q_ciE&query=Jogja%2C%20Yogyakarta%20City%2C%20Special%20Region%20of%20Yogyakarta%2C%20Indonesia&query_understanding_enabled=true&refinement_paths%5B%5D=%2Fhomes&s_tag=M-fveWve&satori_version=1.1.9&screen_height=433&screen_size=large&screen_width=1440&search_type=pagination&section_offset=4&selected_tab_id=home_tab&show_groupings=true&source=mc_search_bar&supports_for_you_v3=true&timezone_offset=420&version=1.6.2`;

  const response = await axios.get(`${SCRAPE_URL}`);

  let data = response.data;
  let explore_tabs = data.explore_tabs[0].sections[0].listings;
  return {
    has_next_page: data.explore_tabs[0].pagination_metadata.has_next_page,
    limit: data.explore_tabs[0].home_tab_metadata.listings_count,
    data: explore_tabs
  };
} 

async function scrape() {
  let result = [];
  let OFFSET = 0;
  let INCREMENT = 50;

  async function cycle() {
    let listings = await page(OFFSET);
    let LIMIT = listings.limit;
    console.log("NEXT PAGE", listings.has_next_page);
    result = result.concat(listings.data);
    if (OFFSET < LIMIT && listings.has_next_page) {
      OFFSET = OFFSET + INCREMENT;
      console.log("CONTINUE", result.length, OFFSET);
      cycle();
    } else if (OFFSET >= LIMIT || !listings.has_next_page) {
      fs.writeFile("./data.json", JSON.stringify(result, null, 2), function(
        err
      ) {});
      console.log("DONE", result.length, OFFSET);
    }
  }

  cycle();
}

scrape();

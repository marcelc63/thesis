let axios = require("axios");
let cheerio = require("cheerio");
let fs = require("fs");
let allListings = require("./longlat.json");

async function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}

async function page(LISTING, LISTINGID, OFFSET) {
  try {
    let SCRAPE_URL = `https://www.airbnb.ae/api/v2/homes_pdp_reviews?currency=USD&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&locale=en&listing_id=${LISTINGID}&_format=for_p3&limit=45&offset=${OFFSET}&order=language_country`;
    const response = await axios.get(`${SCRAPE_URL}`);
    console.log(SCRAPE_URL);
    let data = response.data;
    return {
      success: true,
      count: data.metadata.reviews_count,
      reviews: data.reviews.map(x => {
        return {
          ...x,
          stayInfo: {
            id: LISTING.listing.id,
            name: LISTING.listing.name
          }
        };
      })
    };
  } catch {
    console.log("RETRY");
    await wait(5000);
    return await page(LISTING, LISTINGID, OFFSET);
  }
}

async function scrape(LISTING, OFFSET = 0, COUNTER = 0) {
  // let COUNTER = 0;
  let INCREMENT = 45;
  let LISTINGID = LISTING.listing.id;
  console.log("OFFSET", OFFSET);

  let reviews = await page(LISTING, LISTINGID, OFFSET);
  if (reviews.success) {
    COUNTER = COUNTER + reviews.reviews.length;
    console.log("NEXT PAGE", COUNTER, reviews.reviews.length, reviews.count);
    if (COUNTER < reviews.count) {
      OFFSET = OFFSET + INCREMENT;
      // console.log("LENGTH", COUNTER, reviews.count, reviews.reviews.length);
      console.log("CONTINUE", COUNTER, OFFSET);
      let addition = await scrape(LISTING, OFFSET, COUNTER);
      return reviews.reviews.concat(addition);
    } else if (COUNTER >= reviews.count) {
      COUNTER = 0;
      console.log("DONE", reviews.count);
      return reviews.reviews;
    }
  } else {
    console.log("TIMEOUT");
  }
}

async function start(listings, count, max) {
  console.log("START", count);
  let listing = listings[count];

  console.log(count, "FIRST", listing.listing.name);
  let reviews = await scrape(listing);
  count = count + 1;

  if (count < max) {
    // console.log(reviews);
    let addition = await start(listings, count, max);
    let result = reviews.concat(addition);
    console.log("TOTAL", result.length);
    return result;
  } else if (count >= max) {
    return reviews;
  }
}

async function initiate() {
  let getListings = allListings.filter(x => x.listing.reviews_count !== 0).slice(0,250);
  console.log("Fetching Reviews: ", getListings.length);
  let result = await start(getListings, 0, getListings.length);
  fs.writeFile("./reviews.json", JSON.stringify(result, null, 2), function(
    err
  ) {});
}

initiate();
// console.log(allListings[1].listing.name);

// Import the package main module
const csv = require("csv-parser");
const createCsvWriter = require("csv-writer").createObjectCsvWriter;
const fs = require("fs");
const features = require("./distribution.json");

const csvWriter = createCsvWriter({
  path: "combineddfmratingbinary.csv",
  header: [
    { id: "highrating", title: "highrating" },
    { id: "rating", title: "rating" },
    { id: "document", title: "document" },
    { id: "location", title: "location" },
    { id: "amenity", title: "amenity" },
    { id: "facility", title: "facility" },
    { id: "room", title: "room" },
    { id: "price", title: "price" },
    { id: "recommendation", title: "recommendation" },
    { id: "host", title: "host" },
    { id: "security", title: "security" },
    { id: "community", title: "community" },
  ],
});

const data = [];

fs.createReadStream("reviewtokensdfrating.csv")
  .pipe(csv())
  .on("data", (row) => {
    // console.log(row);

    let values = Object.values(row);
    let newdoc = {
      highrating: values[1],
      rating: values[2],
      document: values[3],
      location: 0,
      amenity: 0,
      facility: 0,
      room: 0,
      price: 0,
      recommendation: 0,
      host: 0,
      security: 0,
      community: 0,
    };
    let entries = Object.entries(row);
    entries.forEach((x) => {
      if (features.location.includes(x[0])) {
        newdoc.location = 1;
      }
      if (features.amenity.includes(x[0])) {
        newdoc.amenity = 1;
      }
      if (features.facility.includes(x[0])) {
        newdoc.facility = 1;
      }
      if (features.room.includes(x[0])) {
        newdoc.room = 1;
      }
      if (features.price.includes(x[0])) {
        newdoc.price = 1;
      }
      if (features.recommendation.includes(x[0])) {
        newdoc.recommendation = 1;
      }
      if (features.host.includes(x[0])) {
        newdoc.host = 1;
      }
      if (features.security.includes(x[0])) {
        newdoc.security = 1;
      }
      if (features.community.includes(x[0])) {
        newdoc.community = 1;
      }
    });
    data.push(newdoc);
    // console.log(values);
    console.log(values[3]);
  })
  .on("end", () => {
    console.log(data);
    csvWriter
      .writeRecords(data)
      .then(() => console.log("The CSV file was written successfully"));
    console.log("CSV file successfully processed");
  });
